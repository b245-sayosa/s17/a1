/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	// function inputInfo(){
	// 	let fullName = prompt("Enter your Full name: ");
	// 	let age = prompt("How old are you? ")
	// 	let location = prompt("Where do you live? ")
	// 	console.log("Hello, "+fullName)
	// 	console.log("You are "+age+" years old")
	// 	console.log("You live in "+location)
	// }

	// inputInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	// function displayFavoriteMusicArtists() {
	// 	let musicArtists = ["1. Eminem", "2. Rihanna", "3. Zack Tabudlo", "4. Arthur Nery", "5. Taylor Swift"]
	// 	console.log(musicArtists[0])
	// 	console.log(musicArtists[1])
	// 	console.log(musicArtists[2])
	// 	console.log(musicArtists[3])
	// 	console.log(musicArtists[4])
	// }

	// displayFavoriteMusicArtists();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	// function displayFavoriteMovies() {
	// 	let favMovies = ["1. Missing", "2. Plane", "3. Avatar:The Way of the Water", "4. Megan", "5. Puss in Boots: The Last Wish"]
	// 	console.log(favMovies[0] + "\nRotten Tomatoes Rating: 85%")
	// 	console.log(favMovies[1] + "\nRotten Tomatoes Rating: 76%")
	// 	console.log(favMovies[2] + "\nRotten Tomatoes Rating: 77%")
	// 	console.log(favMovies[3] + "\nRotten Tomatoes Rating: 95%")
	// 	console.log(favMovies[4] + "\nRotten Tomatoes Rating: 95%")
	// }

	// displayFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);